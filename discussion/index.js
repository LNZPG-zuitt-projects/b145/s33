const express = require('express');
const mongoose = require('mongoose');
const port = 4000;
const app = express();
const taskRoutes = require('./routes/taskRoutes')


app.use(express.json());
app.use(express.urlencoded({extended: true}));

mongoose.connect("mongodb+srv://NeilGopez:Claudia143@b145.daxag.mongodb.net/session33?retryWrites=true&w=majority",
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

let db = mongoose.connection;

db.on('error', console.error.bind(console, "Connection Error"));
db.once('open', () => console.log("Successfully connected to MongoDB"));


app.use('/tasks', taskRoutes)

app.listen(port, () => console.log(`Server running at port ${port}`));
